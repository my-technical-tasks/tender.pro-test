const maxRows = 10;

const loader = (isLoader) => {
  const loader = $('.loader');

  isLoader ? loader.addClass('show') : loader.removeClass('show');
};

const tabRender = (jsonData) => {
  const table = $('.table tbody');
  const rowsArray = $('.table tbody tr');
  const rows = jsonData.map(row => `
        <tr>
          <td>${row.id}</td>
          <td>${row.address}</td>
        </tr>
      `);

  loader(false);
  rowsArray.slice(1).remove();
  table.append(rows.join(''));
};

const getRows = (offset) => {
  const key = '6dea68e23416b21d201571d4c9263a57';
  const url = `https://www.tender.pro/api/_info.companylist_by_set.json?_key=${key}&set_type_id=7&set_id=2&max_rows=${maxRows}&offset=${offset - 1}`;

  loader(true);

  fetch(url)
    .then(response => response.json())
    .then(response => {
      const data = response.result && response.result.data;

      tabRender(data);
    });
};

$(document).ready(() => {
  const pagesRows = $('.pages-info').find('.pages-info__rows');

  pagesRows.text(maxRows);
});
