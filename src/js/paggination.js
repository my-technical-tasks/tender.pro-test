const totalPages = 15;
const paginationItems = 7;

let currentPage = 1;

const range = (start, end) => {
  return Array.from(Array(end - start + 1), (item, i) => i + start);
}

const getPageList = () => {
  const sideWidth = paginationItems < 9 ? 1 : 2;
  const offset = 1;

  if (totalPages <= paginationItems) {
    return range(1, totalPages);
  }

  if (currentPage <= paginationItems - sideWidth - 1 - offset) {
    return range(1, paginationItems - sideWidth - 1)
      .concat([0])
      .concat(range(totalPages - sideWidth + 1, totalPages));

  }

  if (currentPage >= totalPages - sideWidth - 1 - offset) {
    return range(1, sideWidth)
      .concat([0])
      .concat(
        range(totalPages - sideWidth - 3, totalPages)
      );
  }

  return range(1, sideWidth)
    .concat([0])
    .concat(range(currentPage - offset, currentPage + offset))
    .concat([0])
    .concat(range(totalPages - sideWidth + 1, totalPages));
}

const showPage = (whichPage) => {
  if (whichPage < 1 || whichPage > totalPages) return false;
  const currentPageItem = $('.pages-info').find('.pages-info__current');

  currentPage = whichPage;

  currentPageItem.text(whichPage);

  $('.pagination li').slice(1, -1).remove();

  getPageList().forEach(item => {
    $('<li>')
      .addClass(`
        pagination__item
        ${item ? '' : 'separate'}
        ${item === currentPage ? 'active' : ''}
      `)
      .text(item || '...')
      .insertBefore('#pagination__next');
  });

  getRows(whichPage);
}

$(document).ready(() => {
  $('.pagination').append(
    $('<li>').addClass('pagination__item').attr({id: 'pagination__previous'}),
    $('<li>').addClass('pagination__item').attr({id: 'pagination__next'})
  );

  $(document).on('click', '.pagination__item:not(.active):not(.separate)', function() {
    const pageNumber = +$(this).text();
    showPage(pageNumber);
  });

  $('#pagination__next').on('click', function() {
    showPage(currentPage + 1);
  });

  $('#pagination__previous').on('click', function() {
    showPage(currentPage - 1);
  });

  showPage(currentPage);
});
