const gulp = require('gulp');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const prefixes = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const uglify = require('gulp-uglify');
const cssmin = require('gulp-clean-css');
const gcmq = require('gulp-group-css-media-queries');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const browsersync = require('browser-sync');

let path = {
  src: {
    html: "src/template/*.pug",
    css: "src/scss/**/*.scss",
    js: ["src/js/**/*.js", "!src/js/lib/*.*"],
    jsLib: "src/js/lib/*.js",
    img: "src/i/**/*.*",
    font: "src/fonts/**/*.*"
  },
  build: {
    html: "build/",
    css: "build/css/",
    img: "build/i/",
    js: "build/js/",
    jsLib: "build/js/",
    font: "build/fonts/"
  },
  watch: {
    html: "src/template/*.pug",
    css: "src/scss/**/**/*.scss",
    js: ["src/js/**/*.js", "!src/js/lib/"],
    jsLib: ["src/js/lib/**/*.js"],
    img: "src/i/**/*.*"
  }
};

function html() {
  return gulp
    .src(path.src.html)
    .pipe(plumber())
    .pipe(pug({pretty: true}))
    .pipe(gulp.dest(path.build.html))
}

function css() {
  return gulp
    .src(path.src.css)
    // .pipe(concat('style.scss'))
    .pipe(sass({
      includePaths: ['node_modules']
    }))
    .pipe(prefixes())
    .pipe(gcmq())
    .pipe(cssmin())
    .pipe(gulp.dest(path.build.css))
    .pipe(browsersync.stream())
}

function js() {
  return gulp
    .src(path.src.js)
    .pipe(plumber())
    .pipe(concat("main.js"))
    .pipe(babel({
      presets: ["@babel/env"]
    }))
    .pipe(uglify())
    .pipe(gulp.dest(path.build.js))
    .pipe(browsersync.stream())
}

function jsLib() {
  return gulp
    .src(path.src.jsLib)
    .pipe(plumber())
    .pipe(gulp.dest(path.build.jsLib))
    .pipe(browsersync.stream())
}

function img() {
  return gulp
    .src(path.src.img)
    .pipe(plumber())
    .pipe(gulp.dest(path.build.img))
    .pipe(browsersync.stream())
}

function font() {
  return gulp
    .src(path.src.font)
    .pipe(plumber())
    .pipe(gulp.dest(path.build.font))
    .pipe(browsersync.stream())
}

function serve(done) {
  browsersync.init({
    port: 3000,
    watch: true,
    open: false,
    logPrefix: "localhost",
    reloadOnRestart: true,
    server: {
      baseDir: "./build"
    },
  })
  gulp.watch(path.watch.css, css).on('change', browsersync.reload);
  gulp.watch(path.watch.js, js).on('change', browsersync.reload);
  gulp.watch(path.watch.jsLib, jsLib).on('change', browsersync.reload);
  gulp.watch(path.watch.html, html).on('change', browsersync.reload);
  gulp.watch(path.watch.img, img).on('change', browsersync.reload);
  done();
}

const build = gulp.series(css, js, jsLib, html, img, font);
const buildAndStart = gulp.series(html, css, js, jsLib, img, font, serve);

exports.build = build;
exports.default = buildAndStart;
